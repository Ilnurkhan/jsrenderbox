import os
import platform
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


class WebDriver:
    DOWNLOAD_DIR = '/tmp'
    def __init__(self, headless=True):
        self.UAGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36"
        self.options = webdriver.ChromeOptions()
        self.os_type = platform.uname().system
        self.options.add_argument('--disable-extensions')
        if headless:
            self.options.add_argument('--headless')
            self.options.add_argument('--disable-gpu')
            self.options.add_argument('--no-sandbox')
            self.options.add_argument('--user-agent={}'.format(self.UAGENT))

        self.options.add_experimental_option(
            'prefs', {
                'download.default_directory': self.DOWNLOAD_DIR,
                'download.prompt_for_download': False,
                'download.directory_upgrade': True,
                'safebrowsing.enabled': True
            }
        )

    def __enter__(self):
        self.start()
        return self

    def __exit__(self, *args, **kwargs):
        self.close()

    def start(self):
        self.driver = webdriver.Chrome(
            os.path.join(
                os.getcwd(),
                'chromedriver_{}'.format(self.os_type)
                ),
            chrome_options=self.options
            )

    def close(self):
        self.driver.quit()

    def get_page_source(self, url):
        self.driver.get(url)
        return self.driver.page_source

if __name__ == '__main__':
    url = 'https://yandex.ru'
    with WebDriver() as browser:
        html = browser.get_page_source(url)
    print(html)
