import json
import flask

from driver import WebDriver

app = flask.Flask(__name__)
default_name = 'Proxy Render'

@app.route('/')
def index():
    return 'It Works!'

# for requests.post(url, json=)
@app.route('/render', methods=['POST'])
def jsrender():
    if flask.request.headers.get('content-type') == 'application/json':
        json_string = flask.request.get_data().decode('utf-8')
        url = json.loads(json_string)['url']
        with WebDriver() as browser:
            html = browser.get_page_source(url)
        return html
    else:
        flask.abort(403)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8888)
